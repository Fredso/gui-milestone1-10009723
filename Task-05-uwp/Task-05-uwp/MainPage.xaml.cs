﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_05_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /********** UI LOGIC **********/
        public MainPage()
        {
            this.InitializeComponent();
            lblq1.Text = Result.q1;
            lblq2.Text = Result.q2;
            lblq3.Text = Result.q3;
            lblq4.Text = Result.q4;
            lblScore.Text = "0";
            lblRounds.Text = "0";
        }

        private void btnTryNumber_Click(object sender, RoutedEventArgs e)
        {
            Result result = new Result();
            result.CheckNumber(tbxNumber.Text);
            lblMessage.Text = Result.Rmessage;
            lblScore.Text = Result.Score.ToString();
            lblRounds.Text = Result.Rounds.ToString();
        }
    }
    /********** PROGRAM LOGIC **********/

    class Result
    {
        public static int Rounds = 0;
        public static int Score = 0;
        public static string Rmessage;
        public static string q1 = "Try to discover the number. You have 5 attempts:";
        public static string q2 = "Your rounds:";
        public static string q3 = "Your Score:";
        public static string q4 = "Please, introduce a number from 1 to 5:";

        //Method to see if the introduced number is the same as the random one
        public void CheckNumber(string txtnumber)
        {
            int number;
            Random rnd = new Random();
            int rnumber = rnd.Next(1, 6);

            //It checks how many rounds has the user played
            Rounds++;
            if (Rounds <= 5)
            {
                //It checks the introduced numbers
                bool conversion = int.TryParse(txtnumber, out number);
                if (conversion & number > 0 & number < 6)
                {
                    //It checks if the number is right
                    if (number == rnumber)
                    {
                        Score++;
                        Rmessage = $"You did it! {rnumber} was the number.";
                    }
                    else
                    {
                        //result.Score = 1;
                        Rmessage = $"Nope. The number was {rnumber} Try again!";
                    }
                }
                else
                {
                    //result.Score = 1;
                    Rmessage = "Please, enter a proper number next time.";
                }
            }
            else
            {
                Rounds = 5;
                Rmessage = "Sorry, you reached 5 attempts." + '\n' +
                    "You cannot continue playing.";
            }
        }
    }
}
