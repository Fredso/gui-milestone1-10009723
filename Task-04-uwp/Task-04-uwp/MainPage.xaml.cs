﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_04_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /********** UI LOGIC **********/
        public MainPage()
        {
            this.InitializeComponent();
            lblq1.Text = q1;
        }

        private void btnCheckFruit_Click(object sender, RoutedEventArgs e)
        {
            lblAnswer.Text = CheckFruit(tbxFruit.Text);
        }

        /********** PROGRAM LOGIC **********/

        static string q1 = "Please, introduce a fruit to see if it is in the dictionary:";

        //Dictionary definition
        static Dictionary<string, string> CreateDictionary()
        {
            var VF = new Dictionary<string, string>();
            VF.Add("banana", "fruit");
            VF.Add("kiwi", "fruit");
            VF.Add("apple", "fruit");
            VF.Add("pear", "fruit");
            VF.Add("orange", "fruit");
            VF.Add("tomato", "vegetable");
            VF.Add("cucumber", "vegetable");
            VF.Add("capsicum", "vegetable");
            VF.Add("onion", "vegetable");
            return VF;
        }

        //It checks if the fruit is in the dictionary
        static string CheckFruit(string vf)
        {
            var Answer = "";
            var FruitsDictionary = CreateDictionary();
            if (FruitsDictionary.ContainsKey(vf.ToLower()))
            {
                Answer = $"The {vf} is in the dictionary and it is a {FruitsDictionary[vf.ToLower()]}.";
            }
            else
            {
                Answer = $"The {vf} is not in the dictionary, so I don't know what it is.";
            }
            return Answer;
        }

    }
}
