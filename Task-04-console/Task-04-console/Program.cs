﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04_console
{
    class Program
    {
        /********** UI LOGIC **********/
        static void Main(string[] args)
        {
            Console.WriteLine(q1);
            Console.WriteLine(CheckFruit(Console.ReadLine()));
            Console.WriteLine("Please, press enter to finish");
            Console.ReadLine();
        }


        /********** PROGRAM LOGIC **********/

        static string q1 = "Please, introduce a fruit to see if it is in the dictionary:";

        //Dictionary definition
        static Dictionary<string, string> CreateDictionary()
        {
            var VF = new Dictionary<string, string>();
            VF.Add("banana", "fruit");
            VF.Add("kiwi", "fruit");
            VF.Add("apple", "fruit");
            VF.Add("pear", "fruit");
            VF.Add("orange", "fruit");
            VF.Add("tomato", "vegetable");
            VF.Add("cucumber", "vegetable");
            VF.Add("capsicum", "vegetable");
            VF.Add("onion", "vegetable");
            return VF;
        }

        //It checks if the fruit is in the dictionary
        static string CheckFruit(string vf)
        {
            var Answer = "";
            var FruitsDictionary = CreateDictionary();
            if (FruitsDictionary.ContainsKey(vf.ToLower()))
            {
                Answer = $"The {vf} is in the dictionary and it is a {FruitsDictionary[vf.ToLower()]}.";
            }
            else
            {
                Answer = $"The {vf} is not in the dictionary, so I don't know what it is.";
            }
            return Answer;
        }
    }
}
