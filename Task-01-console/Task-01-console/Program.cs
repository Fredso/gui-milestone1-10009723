﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01_console
{
    class Program
    {
        /********** UI LOGIC **********/
        static void Main(string[] args)
        {
            //It asks for the distance to convert
            Console.WriteLine(q1);
            var distancetxt= Console.ReadLine();
            //It asks for the type of conversion
            Console.WriteLine(q2);
            var conversiontype = Console.ReadLine();
            //It writes the result
            Console.WriteLine(ConvertDistance(conversiontype, distancetxt));
            Console.WriteLine("Please, press enter to finish");
            Console.ReadLine();
        }

        /********** PROGRAM LOGIC **********/

        static string q1 = "Please enter the distance to be converted.";
        static string q2 = "Please enter 0 to convert from Km to Miles and 1 from Miles to Km.";

        //It checks data integrity, converts the distance and returns the string result
        static string ConvertDistance(string ctxt, string dtxt)
        {
            double distance;
            int option;
            string answer = "";
            //It checks the introduced distance to be converted
            bool resultd = double.TryParse(dtxt, out distance);
            bool resultc = int.TryParse(ctxt, out option);
            if (resultc&resultd)
            {
                switch (option)
                {
                    //Conversion from Km to miles
                    case 0:
                        answer = $"The distance {distance} Km converted is {distance * 0.62137119} miles.";
                        break;
                    //Conversion from miles to Km
                    case 1:
                        answer = $"The distance {distance} miles converted is {distance * 1.609344} Km.";
                        break;
                    default:
                        answer = "That is not a valid option. Try again next time!";
                        break;
                }
            }
            else
            {
                answer = "The data introduced is not correct. Try again next time!";
            }
            return answer;
        }
    }
}
