﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03_console
{
    class Program
    {
        /********** UI LOGIC **********/
        static void Main(string[] args)
        {
            Console.WriteLine(q1);
            var i = 0;
            foreach (string fruit in Fruits)
            {
                Console.WriteLine($"{i}. {fruit}");
                i++;
            }
            Console.WriteLine(DisplaySelection(Console.ReadLine()));
            Console.WriteLine("Please, press enter to finish");
            Console.ReadLine();
        }

        /********** PROGRAM LOGIC **********/

        static string q1 = "Please select a fruit:";
        static string[] Fruits = new string[4] { "apple", "pear", "banana", "feijoa" };

        static string DisplaySelection(string selectiontxt)
        {
            int selection;
            var answer = "";
            bool result = int.TryParse(selectiontxt, out selection);
            if (result & selection < Fruits.GetLength(0))
            {
                answer= $"The fruit selected is {Fruits[selection]}.";
            }
            else
            {
                answer="That is not a valid option. Try again next time!";
            }
            return answer;
        }
    }
}
