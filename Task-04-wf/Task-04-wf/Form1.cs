﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_04_wf
{
    public partial class Form1 : Form
    {
        /********** UI LOGIC **********/
        public Form1()
        {
            InitializeComponent();
            lblq1.Text = q1;
        }

        private void btnCheckFruit_Click(object sender, EventArgs e)
        {
            lblAnswer.Text = CheckFruit(tbxFruit.Text);
        }

        /********** PROGRAM LOGIC **********/

        static string q1 = "Please, introduce a fruit to see if it is in the dictionary:";

        //Dictionary definition
        static Dictionary<string, string> CreateDictionary()
        {
            var VF = new Dictionary<string, string>();
            VF.Add("banana", "fruit");
            VF.Add("kiwi", "fruit");
            VF.Add("apple", "fruit");
            VF.Add("pear", "fruit");
            VF.Add("orange", "fruit");
            VF.Add("tomato", "vegetable");
            VF.Add("cucumber", "vegetable");
            VF.Add("capsicum", "vegetable");
            VF.Add("onion", "vegetable");
            return VF;
        }

        //It checks if the fruit is in the dictionary
        static string CheckFruit( string vf)
        {
            var Answer = "";
            var FruitsDictionary = CreateDictionary();
            if (FruitsDictionary.ContainsKey(vf.ToLower()))
            {
                Answer = $"The {vf} is in the dictionary and it is a {FruitsDictionary[vf.ToLower()]}.";
            }
            else
            {
                Answer = $"The {vf} is not in the dictionary, so I don't know what it is.";
            }
            return Answer;
        }

    }
}
