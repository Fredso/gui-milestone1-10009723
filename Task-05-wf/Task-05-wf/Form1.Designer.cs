﻿namespace Task_05_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTryNumber = new System.Windows.Forms.Button();
            this.lblq4 = new System.Windows.Forms.Label();
            this.tbxNumber = new System.Windows.Forms.TextBox();
            this.lblMessage = new System.Windows.Forms.Label();
            this.lblq3 = new System.Windows.Forms.Label();
            this.lblScore = new System.Windows.Forms.Label();
            this.lblq1 = new System.Windows.Forms.Label();
            this.lblRounds = new System.Windows.Forms.Label();
            this.lblq2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnTryNumber
            // 
            this.btnTryNumber.Location = new System.Drawing.Point(174, 139);
            this.btnTryNumber.Name = "btnTryNumber";
            this.btnTryNumber.Size = new System.Drawing.Size(66, 30);
            this.btnTryNumber.TabIndex = 0;
            this.btnTryNumber.Text = "Try";
            this.btnTryNumber.UseVisualStyleBackColor = true;
            this.btnTryNumber.Click += new System.EventHandler(this.btnTryNumber_Click);
            // 
            // lblq4
            // 
            this.lblq4.AutoSize = true;
            this.lblq4.Location = new System.Drawing.Point(11, 34);
            this.lblq4.Name = "lblq4";
            this.lblq4.Size = new System.Drawing.Size(29, 13);
            this.lblq4.TabIndex = 1;
            this.lblq4.Text = "lblq4";
            // 
            // tbxNumber
            // 
            this.tbxNumber.Location = new System.Drawing.Point(205, 31);
            this.tbxNumber.Name = "tbxNumber";
            this.tbxNumber.Size = new System.Drawing.Size(35, 20);
            this.tbxNumber.TabIndex = 2;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(11, 60);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(0, 13);
            this.lblMessage.TabIndex = 3;
            // 
            // lblq3
            // 
            this.lblq3.AutoSize = true;
            this.lblq3.Location = new System.Drawing.Point(11, 126);
            this.lblq3.Name = "lblq3";
            this.lblq3.Size = new System.Drawing.Size(29, 13);
            this.lblq3.TabIndex = 4;
            this.lblq3.Text = "lblq3";
            // 
            // lblScore
            // 
            this.lblScore.AutoSize = true;
            this.lblScore.Location = new System.Drawing.Point(96, 126);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(0, 13);
            this.lblScore.TabIndex = 5;
            // 
            // lblq1
            // 
            this.lblq1.AutoSize = true;
            this.lblq1.Location = new System.Drawing.Point(7, 9);
            this.lblq1.Name = "lblq1";
            this.lblq1.Size = new System.Drawing.Size(29, 13);
            this.lblq1.TabIndex = 6;
            this.lblq1.Text = "lblq1";
            // 
            // lblRounds
            // 
            this.lblRounds.AutoSize = true;
            this.lblRounds.Location = new System.Drawing.Point(96, 103);
            this.lblRounds.Name = "lblRounds";
            this.lblRounds.Size = new System.Drawing.Size(0, 13);
            this.lblRounds.TabIndex = 8;
            // 
            // lblq2
            // 
            this.lblq2.AutoSize = true;
            this.lblq2.Location = new System.Drawing.Point(11, 103);
            this.lblq2.Name = "lblq2";
            this.lblq2.Size = new System.Drawing.Size(29, 13);
            this.lblq2.TabIndex = 7;
            this.lblq2.Text = "lblq2";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(252, 181);
            this.Controls.Add(this.lblRounds);
            this.Controls.Add(this.lblq2);
            this.Controls.Add(this.lblq1);
            this.Controls.Add(this.lblScore);
            this.Controls.Add(this.lblq3);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.tbxNumber);
            this.Controls.Add(this.lblq4);
            this.Controls.Add(this.btnTryNumber);
            this.Name = "Form1";
            this.Text = "Game";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTryNumber;
        private System.Windows.Forms.Label lblq4;
        private System.Windows.Forms.TextBox tbxNumber;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblq3;
        private System.Windows.Forms.Label lblScore;
        private System.Windows.Forms.Label lblq1;
        private System.Windows.Forms.Label lblRounds;
        private System.Windows.Forms.Label lblq2;
    }
}

