﻿namespace Task_04_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCheckFruit = new System.Windows.Forms.Button();
            this.tbxFruit = new System.Windows.Forms.TextBox();
            this.lblAnswer = new System.Windows.Forms.Label();
            this.lblq1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCheckFruit
            // 
            this.btnCheckFruit.Location = new System.Drawing.Point(218, 220);
            this.btnCheckFruit.Name = "btnCheckFruit";
            this.btnCheckFruit.Size = new System.Drawing.Size(77, 29);
            this.btnCheckFruit.TabIndex = 6;
            this.btnCheckFruit.Text = "Check";
            this.btnCheckFruit.UseVisualStyleBackColor = true;
            this.btnCheckFruit.Click += new System.EventHandler(this.btnCheckFruit_Click);
            // 
            // tbxFruit
            // 
            this.tbxFruit.Location = new System.Drawing.Point(17, 42);
            this.tbxFruit.Name = "tbxFruit";
            this.tbxFruit.Size = new System.Drawing.Size(99, 20);
            this.tbxFruit.TabIndex = 5;
            // 
            // lblAnswer
            // 
            this.lblAnswer.AutoSize = true;
            this.lblAnswer.Location = new System.Drawing.Point(20, 80);
            this.lblAnswer.Name = "lblAnswer";
            this.lblAnswer.Size = new System.Drawing.Size(0, 13);
            this.lblAnswer.TabIndex = 3;
            // 
            // lblq1
            // 
            this.lblq1.AutoSize = true;
            this.lblq1.Location = new System.Drawing.Point(14, 26);
            this.lblq1.Name = "lblq1";
            this.lblq1.Size = new System.Drawing.Size(29, 13);
            this.lblq1.TabIndex = 4;
            this.lblq1.Text = "lblq1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(307, 261);
            this.Controls.Add(this.btnCheckFruit);
            this.Controls.Add(this.tbxFruit);
            this.Controls.Add(this.lblAnswer);
            this.Controls.Add(this.lblq1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCheckFruit;
        private System.Windows.Forms.TextBox tbxFruit;
        private System.Windows.Forms.Label lblAnswer;
        private System.Windows.Forms.Label lblq1;
    }
}

